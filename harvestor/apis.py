from django.shortcuts import get_object_or_404
from django.shortcuts import render
from .models import *
from django.views import generic
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *

@api_view(['GET', 'POST'])
def news_count_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        news = News.objects.all().count()
        return Response(news)
    return Response("serializer.errors", status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def news_list_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        news = News.objects.all()
        serializer = NewsSerializer(news, many=True)
        print("###########################################")
        print(serializer)
        print("###########################################")
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def news_detail_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        _id = request.GET.get('id')
        news = News.objects.get(id=_id)
        serializer = NewsSerializer(news)
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
