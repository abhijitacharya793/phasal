# Generated by Django 3.0.3 on 2020-02-16 23:32

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, help_text='Unique ID for this particular post', primary_key=True, serialize=False)),
                ('uid', models.CharField(help_text='Enter Username', max_length=100)),
                ('title', models.CharField(help_text='Enter title', max_length=500)),
                ('photo', models.ImageField(null=True, upload_to='media/')),
                ('summary', models.TextField(help_text='Enter post summary', max_length=2000, null=True)),
                ('date_of_post', models.DateField(auto_now_add=True)),
            ],
            options={
                'ordering': ['title'],
            },
        ),
    ]
