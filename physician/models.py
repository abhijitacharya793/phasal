from django.db import models
from django.urls import reverse # Used to generate URLs by reversing the URL patterns
import uuid # Required for unique instances

# Create your models here.
"""<<--__ PROBLEM PICTURE POST __-->>"""
class Report(models.Model):
    """Model representing a Post."""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID for this particular post')
    uid = models.CharField(null=True,max_length=100, help_text='Enter Username')
    title = models.CharField(max_length=500, help_text='Enter title')
    summary = models.TextField(max_length=2000, help_text='Enter post summary',null=True)
    photo = models.ImageField(null=True, upload_to='diseasedpics/')
    prediction = models.TextField(max_length=100, null=True, blank=True)

    date_of_post = models.DateField(auto_now_add=True)

    def __str__(self):
        """String for representing the Model object."""
        return self.title

    def get_absolute_url(self):
        """Returns the url to access a detail record for this report."""
        return reverse('report-detail')

    class Meta:
        ordering = ['title']
