# Generated by Django 3.0.3 on 2020-05-13 14:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('harvestor', '0005_remove_news_content'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='content',
            field=models.TextField(default=None, help_text='Enter article content', max_length=10000),
        ),
    ]
