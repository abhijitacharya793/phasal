from django.urls import path
from . import views
from . import apis

urlpatterns = [
    #HTML response urls
    path('', views.index, name='index_harvestor'),
    path('news/', views.news_list_view, name='news'),
    path('news_detail/', views.news_detail_view, name='news-detail'),

    #APIS
    path('api/news_count/', apis.news_count_api, name='news_count_api'),
    path('api/news/', apis.news_list_view_api, name='news_api'),
    path('api/news_detail/', apis.news_detail_view_api, name='news-detail_api'),
]
