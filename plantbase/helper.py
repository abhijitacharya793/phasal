from plantbase.models import *

def return_count():
    # Generate counts of some of the main objects
    num_crops = Crop.objects.all().count()
    num_diseases = Disease.objects.all().count()
    num_soil_zones = Soil_Zone.objects.all().count()
    num_districts = District.objects.all().count()
    num_seasons = Season.objects.all().count()

    context = {
        'num_crops': num_crops,
        'num_diseases': num_diseases,
        'num_soil_zones': num_soil_zones,
        'num_districts': num_districts,
        'num_seasons': num_seasons,
    }
    return context
