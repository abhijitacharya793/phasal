from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.http import HttpResponse, Http404
from plantbase.models import *
from .helper import *
from django.views import generic

# Create your views here.
def index(request):
    """View function for home page of site."""

    context = return_count()
    return render(request, 'plantbase/index.html', context=context)

def crop_list_view(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        crop_list = Crop.objects.all()
        print(crop_list)
    except Crop.DoesNotExist:
        raise Http404('Crop does not exist')

    return render(request, 'plantbase/crop_list.html', context={'crop_list': crop_list})

def crop_detail_view(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _name = request.GET.get('name')
    try:
        crop = Crop.objects.get(name=_name)
        print(crop)
    except Crop.DoesNotExist:
        raise Http404('Crop does not exist')

    return render(request, 'plantbase/crop_detail.html', context={'crop': crop})

def soil_list_view(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        soil_list = Soil_Zone.objects.all()
        print(soil_list)
    except Soil_Zone.DoesNotExist:
        raise Http404('Soil Zone does not exist')

    return render(request, 'plantbase/soil_list.html', context={'soil_list': soil_list})

def soil_detail_view(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _name = request.GET.get('name')
    try:
        soil = Soil_Zone.objects.get(name=_name)
        print(soil)
    except Soil_Zone.DoesNotExist:
        raise Http404('Soil Zone does not exist')

    return render(request, 'plantbase/soil_detail.html', context={'soil': soil})

def disease_list_view(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        disease_list = Disease.objects.all()
        print(disease_list)
    except Disease.DoesNotExist:
        raise Http404('Disease does not exist')

    return render(request, 'plantbase/disease_list.html', context={'disease_list': disease_list})

def disease_detail_view(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _name = request.GET.get('name')
    try:
        disease = Disease.objects.get(name=_name)
        print(disease)
    except Disease.DoesNotExist:
        raise Http404('Disease does not exist')

    return render(request, 'plantbase/disease_detail.html', context={'disease': disease})

def season_list_view(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        season_list = Season.objects.all()
        print(season_list)
    except Season.DoesNotExist:
        raise Http404('Season does not exist')

    return render(request, 'plantbase/season_list.html', context={'season_list': season_list})

def season_detail_view(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _name = request.GET.get('name')
    try:
        season = Season.objects.get(name=_name)
        print(season)
    except Season.DoesNotExist:
        raise Http404('Season does not exist')

    return render(request, 'plantbase/season_detail.html', context={'season': season})

def district_list_view(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        district_list = District.objects.all()
        print(district_list)
    except District.DoesNotExist:
        raise Http404('District does not exist')

    return render(request, 'plantbase/district_list.html', context={'district_list': district_list})

def district_detail_view(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _name = request.GET.get('name')
    try:
        district = District.objects.get(name=_name)
        print(district)
    except District.DoesNotExist:
        raise Http404('District does not exist')

    return render(request, 'plantbase/district_detail.html', context={'district': district})
