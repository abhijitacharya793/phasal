from django.urls import path
from . import views
from . import apis

urlpatterns = [
    path('', views.index, name='index_plantbase'),
    #HTML response urls
    path('crops/', views.crop_list_view, name='crops'),
    path('crop/', views.crop_detail_view, name='crop-detail'),
    path('soil_zones/', views.soil_list_view, name='soil_zones'),
    path('soil_zone/', views.soil_detail_view, name='zone-detail'),
    path('diseases/', views.disease_list_view, name='diseases'),
    path('disease/', views.disease_detail_view, name='disease-detail'),
    path('districts/', views.district_list_view, name='districts'),
    path('district/', views.district_detail_view, name='district-detail'),
    path('seasons/', views.season_list_view, name='seasons'),
    path('season/', views.season_detail_view, name='season-detail'),
    #APIS
    path('api/crop_count/', apis.crop_count_api, name='crop_count_api'),
    path('api/crops/', apis.crop_list_view_api, name='crops_api'),
    path('api/crop/', apis.crop_detail_view_api, name='crop-detail_api'),
    path('api/soil_zone_count/', apis.soil_count_api, name='soil_count_api'),
    path('api/soil_zones/', apis.soil_list_view_api, name='soil_zones_api'),
    path('api/soil_zone/', apis.soil_detail_view_api, name='zone-detail_api'),
    path('api/disease_count/', apis.disease_count_api, name='disease_count_api'),
    path('api/diseases/', apis.disease_list_view_api, name='diseases_api'),
    path('api/disease/', apis.disease_detail_view_api, name='disease-detail_api'),
    path('api/district_count/', apis.district_count_api, name='district_count_api'),
    path('api/districts/', apis.district_list_view_api, name='districts_api'),
    path('api/district/', apis.district_detail_view_api, name='district-detail_api'),
    path('api/season_count/', apis.season_count_api, name='season_count_api'),
    path('api/seasons/', apis.season_list_view_api, name='seasons_api'),
    path('api/season/', apis.season_detail_view_api, name='season-detail_api'),
]
# http://abhijitacharya.pythonanywhere.com/plantbase/api/crops/
# http://abhijitacharya.pythonanywhere.com/plantbase/api/crop/?name=rice
# http://abhijitacharya.pythonanywhere.com/plantbase/api/soil_zones/
# http://abhijitacharya.pythonanywhere.com/plantbase/api/soil_zone/?name=<soil_zone_name>
# http://abhijitacharya.pythonanywhere.com/plantbase/api/diseases/
# http://abhijitacharya.pythonanywhere.com/plantbase/api/disease/?name=<disease_name>
# http://abhijitacharya.pythonanywhere.com/plantbase/api/districts/
# http://abhijitacharya.pythonanywhere.com/plantbase/api/district/?name=<district_name>
# http://abhijitacharya.pythonanywhere.com/plantbase/api/seasons/
# http://abhijitacharya.pythonanywhere.com/plantbase/api/season/?name=<season_name>
