from django.db import models
from django.urls import reverse # Used to generate URLs by reversing the URL patterns
import uuid # Required for unique instances

# Create your models here.
"""<<--__ CROP __-->>"""
class Crop(models.Model):
    """Model representing a Crop."""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID for this particular crop')
    name = models.CharField(max_length=200, help_text='Enter crop name')
    summary = models.TextField(max_length=1000, help_text='Enter a brief description of the crop')
    soil_zone = models.ManyToManyField('Soil_Zone', help_text='Select soil zones for this crop')
    season = models.ForeignKey('Season', on_delete=models.SET_NULL, null=True)
    name_hi = models.CharField(max_length=200, help_text='Enter crop name')
    summary_hi = models.TextField(max_length=1000, help_text='Enter a brief description of the crop')
    croppic = models.ImageField(null=True, upload_to='croppics/')

    def __str__(self):
        """String for representing the Model object."""
        return self.name

    def get_absolute_url(self):
        """Returns the url to access a detail record for this crop."""
        return reverse('crop-detail')

    def display_soil_zone(self):
        """Create a string for the Soil Zone. This is required to display soil zone in Admin."""
        return ', '.join(soil_zone.name for soil_zone in self.soil_zone.all())

    display_soil_zone.short_description = 'Soil_Zone'

    class Meta:
        ordering = ['name']

"""<<--__ DISEASE __-->>"""
class Disease(models.Model):
    """Model representing a crop disease."""
    name = models.CharField(max_length=200, help_text='Enter a crop disease')
    crop = models.ManyToManyField('Crop', help_text='Select crop')
    description = models.TextField(max_length=1000, help_text='Enter description')
    type = models.CharField(max_length=200, help_text='Enter type of disease')
    diagnosis = models.TextField(max_length=1000, help_text='Enter diagnosis')
    causes = models.TextField(max_length=1000, help_text='Enter causes')
    prevention = models.TextField(max_length=1000, help_text='Enter prevention')
    treatment = models.TextField(max_length=1000, help_text='Enter treatment methods')
    STAGE_NAME = (
        ('s', 'Seeding Stage'),
        ('v', 'Vegetative Stage'),
        ('fl', 'Flowering Stage'),
        ('fr', 'Fruiting Stage'),
        ('h', 'Harvesting Stage'),
    )
    stage = models.CharField(
        max_length=2,
        choices=STAGE_NAME,
        blank=True,
        default=None,
        help_text='Stage of crop growth',
    )
    name_hi = models.CharField(max_length=200, help_text='Enter a crop disease')
    description_hi = models.TextField(max_length=1000, help_text='Enter description')
    type_hi = models.CharField(max_length=200, help_text='Enter type of disease')
    diagnosis_hi = models.TextField(max_length=1000, help_text='Enter diagnosis')
    causes_hi = models.TextField(max_length=1000, help_text='Enter causes')
    prevention_hi = models.TextField(max_length=1000, help_text='Enter prevention')
    treatment_hi = models.TextField(max_length=1000, help_text='Enter treatment methods')
    STAGE_NAME_HI = (
        ('s', 'बुवाई का चरण'),
        ('v', 'वनस्पति अवस्था'),
        ('fl', 'पुष्पन अवस्था'),
        ('fr', 'फलने की अवस्था'),
        ('h', 'कटाई की अवस्था'),
    )
    stage_hi = models.CharField(
        max_length=2,
        choices=STAGE_NAME_HI,
        blank=True,
        default=None,
        help_text='Stage of crop growth',
    )
    diseasepic = models.ImageField(null=True, upload_to='diseasepics/')

    def __str__(self):
        """String for representing the Model object."""
        return self.name

    def get_absolute_url(self):
        """Returns the url to access a detail record for this disease."""
        return reverse('disease-detail')

    def display_crop(self):
        """Create a string for the Crop. This is required to display crop in Admin."""
        return ', '.join(crop.name for crop in self.crop.all()[:3])

    display_crop.short_description = 'Crop'

"""<<--__ SOIL ZONE __-->>"""
class Soil_Zone(models.Model):
    """Model representing a soil zone."""
    name = models.CharField(max_length=200, help_text='Enter a soil zone')
    characteristics = models.TextField(max_length=1000, help_text='Enter description')
    name_hi = models.CharField(max_length=200, help_text='Enter a soil zone')
    characteristics_hi = models.TextField(max_length=1000, help_text='Enter description')

    def __str__(self):
        """String for representing the Model object."""
        return self.name

    def get_absolute_url(self):
        """Returns the url to access a detail record for this zone."""
        return reverse('zone-detail')

"""<<--__ DISTRICT __-->>"""
class District(models.Model):
    """Model representing a district."""
    name = models.CharField(max_length=200, help_text='Enter a district name')
    soil_zone = models.ForeignKey('Soil_Zone', on_delete=models.SET_NULL, null=True)
    name_hi = models.CharField(max_length=200, help_text='Enter a district name')

    def __str__(self):
        """String for representing the Model object."""
        return self.name

    def get_absolute_url(self):
        """Returns the url to access a detail record for this season."""
        return reverse('district-detail')


"""<<--__ SEASON __-->>"""
class Season(models.Model):
    """Model representing a crop season."""
    name = models.CharField(max_length=100)
    start_month = models.CharField(max_length=200, help_text='Enter start month',null=True)
    end_month = models.CharField(max_length=200, help_text='Enter end month',null=True)
    name_hi = models.CharField(max_length=100)

    def __str__(self):
        """String for representing the Model object."""
        return self.name

    def get_absolute_url(self):
        """Returns the url to access a detail record for this season."""
        return reverse('season-detail')
