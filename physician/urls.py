from django.urls import path
from . import views
from . import apis

urlpatterns = [
    #HTML response urls
    path('', views.index, name='index_physician'),
    path('success', views.success, name = 'success'),
    path('upload_photo', views.upload_photo, name = 'upload_photo'),
    path('reports', views.report_list_view, name = 'reports'),
    path('report', views.report_detail_view, name = 'report-detail'),

    #APIS
    # path('api/upload_photo', apis.upload_photo_api.as_view(), name='upload_photo_api'),
    path('api/upload_photo/', apis.upload_photo_api.as_view(), name='upload_photo_api'),
    path('api/report_count/', apis.report_count_api, name='report_count_api'),
    path('api/reports/', apis.report_list_view_api, name='reports_api'),
    path('api/report/', apis.report_detail_view_api, name='report-detail_api'),
    path('api/reportuid/', apis.report_user_list_view_api, name='reportuid-detail_api'),

]
