from django.contrib import admin
from .models import *

# Register your models here.

class ReportAdmin(admin.ModelAdmin):
    list_display = ('id','uid','title','summary','photo','date_of_post')
    list_filter = ('uid', 'date_of_post')

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID for this particular post')
    uid = models.CharField(null=True,max_length=100, help_text='Enter Username')
    title = models.CharField(max_length=500, help_text='Enter title')
    summary = models.TextField(max_length=2000, help_text='Enter post summary',null=True)
    photo = models.ImageField(null=True, upload_to='diseasedpics/')

    date_of_post = models.DateField(auto_now_add=True)

admin.site.register(Report,ReportAdmin)
