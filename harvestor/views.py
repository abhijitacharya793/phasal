from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from .models import *
import threading

# Create your views here.
# Index
def index(request):
    return render(request, 'harvestor/index.html')

def news_list_view(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        news_list = News.objects.all()
        print(news_list)
    except News.DoesNotExist:
        raise Http404('News does not exist')

    return render(request, 'harvestor/news_list.html', context={'news_list': news_list})

def news_detail_view(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _id = request.GET.get('id')
    try:
        news = News.objects.get(id=_id)
        print(news)
    except News.DoesNotExist:
        raise Http404('News does not exist')

    return render(request, 'harvestor/news_detail.html', context={'news': news})
