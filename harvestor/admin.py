from django.contrib import admin
from .models import *

# Register your models here.
class NewsAdmin(admin.ModelAdmin):
    list_display = ('title','meta_description','date','meta_keyword',)
    list_filter = ('title', 'meta_keyword')

admin.site.register(News,NewsAdmin)
