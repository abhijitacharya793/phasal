from django.contrib import admin
from .models import *

# Register your models here.
class CropAdmin(admin.ModelAdmin):
    list_display = ('name','season','display_soil_zone','croppic',)
    list_filter = ('season', 'soil_zone')

class DiseaseAdmin(admin.ModelAdmin):
    list_display = ('name','display_crop','type','stage',)
    list_filter = ('crop', 'stage')

class Soil_ZoneAdmin(admin.ModelAdmin):
    list_display = ('name',)

class DistrictAdmin(admin.ModelAdmin):
    list_display = ('name','soil_zone',)
    list_filter = ('soil_zone',)

class SeasonAdmin(admin.ModelAdmin):
    list_display = ('name','start_month','end_month',)

admin.site.register(Crop,CropAdmin)
admin.site.register(Disease,DiseaseAdmin)
admin.site.register(Soil_Zone,Soil_ZoneAdmin)
admin.site.register(District,DistrictAdmin)
admin.site.register(Season,SeasonAdmin)
