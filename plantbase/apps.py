from django.apps import AppConfig


class PlantbaseConfig(AppConfig):
    name = 'plantbase'
