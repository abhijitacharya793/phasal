from django.shortcuts import get_object_or_404
from django.shortcuts import render
from plantbase.models import *
from .helper import *
from django.views import generic
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *

@api_view(['GET', 'POST'])
def crop_count_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        crops = Crop.objects.all().count()
        return Response(crops)
    return Response("serializer.errors", status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def crop_list_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        crops = Crop.objects.all()
        serializer = CropSerializer(crops, many=True)
        print("###########################################")
        print(serializer)
        print("###########################################")
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def crop_detail_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        _name = request.GET.get('name')
        crops = Crop.objects.get(name=_name)
        serializer = CropSerializer(crops)
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def disease_count_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        diseases = Disease.objects.all().count()
        return Response(diseases)
    return Response("serializer.errors", status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def disease_list_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        diseases = Disease.objects.all()
        serializer = DiseaseSerializer(diseases, many=True)
        print("###########################################")
        print(serializer)
        print("###########################################")
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def disease_detail_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        _name = request.GET.get('name')
        diseases = Disease.objects.get(name=_name)
        serializer = DiseaseSerializer(diseases)
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def soil_count_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        soil_zones = Soil_Zone.objects.all().count()
        return Response(soil_zones)
    return Response("serializer.errors", status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def soil_list_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        soil_zones = Soil_Zone.objects.all()
        serializer = Soil_ZoneSerializer(soil_zones, many=True)
        print("###########################################")
        print(serializer)
        print("###########################################")
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def soil_detail_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        _name = request.GET.get('name')
        soil_zones = Soil_Zone.objects.get(name=_name)
        serializer = Soil_ZoneSerializer(soil_zones)
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def district_count_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        districts = District.objects.all().count()
        return Response(districts)
    return Response("serializer.errors", status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def district_list_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        districts = District.objects.all()
        serializer = DistrictSerializer(districts, many=True)
        print("###########################################")
        print(serializer)
        print("###########################################")
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def district_detail_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        _name = request.GET.get('name')
        districts = District.objects.get(name=_name)
        serializer = DistrictSerializer(districts)
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def season_count_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        seasons = Season.objects.all().count()
        return Response(seasons)
    return Response("serializer.errors", status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def season_list_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        seasons = Season.objects.all()
        serializer = SeasonSerializer(seasons, many=True)
        print("###########################################")
        print(serializer)
        print("###########################################")
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def season_detail_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        _name = request.GET.get('name')
        seasons = Season.objects.get(name=_name)
        serializer = SeasonSerializer(seasons)
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


"""
# Create your views here.
def index(request):

    context = return_count()
    return render(request, 'plantbase/index.html', context=context)
def crop_list_view_api(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        crop_list = Crop.objects.all()
        print(crop_list)
    except Crop.DoesNotExist:
        raise Http404('Crop does not exist')

    return render(request, 'plantbase/crop_list.html', context={'crop_list': crop_list})

def crop_detail_view_api(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _name = request.GET.get('name')
    try:
        crop = Crop.objects.get(name=_name)
        print(crop)
    except Crop.DoesNotExist:
        raise Http404('Crop does not exist')

    return render(request, 'plantbase/crop_detail.html', context={'crop': crop})
def soil_list_view_api(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        soil_list = Soil_Zone.objects.all()
        print(soil_list)
    except Soil_Zone.DoesNotExist:
        raise Http404('Soil Zone does not exist')

    return render(request, 'plantbase/soil_list.html', context={'soil_list': soil_list})

def soil_detail_view_api(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _name = request.GET.get('name')
    try:
        soil = Soil_Zone.objects.get(name=_name)
        print(soil)
    except Soil_Zone.DoesNotExist:
        raise Http404('Soil Zone does not exist')

    return render(request, 'plantbase/soil_detail.html', context={'soil': soil})

def disease_list_view_api(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        disease_list = Disease.objects.all()
        print(disease_list)
    except Disease.DoesNotExist:
        raise Http404('Disease does not exist')

    return render(request, 'plantbase/disease_list.html', context={'disease_list': disease_list})

def disease_detail_view_api(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _name = request.GET.get('name')
    try:
        disease = Disease.objects.get(name=_name)
        print(disease)
    except Disease.DoesNotExist:
        raise Http404('Disease does not exist')

    return render(request, 'plantbase/disease_detail.html', context={'disease': disease})

def season_list_view_api(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        season_list = Season.objects.all()
        print(season_list)
    except Season.DoesNotExist:
        raise Http404('Season does not exist')

    return render(request, 'plantbase/season_list.html', context={'season_list': season_list})

def season_detail_view_api(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _name = request.GET.get('name')
    try:
        season = Season.objects.get(name=_name)
        print(season)
    except Season.DoesNotExist:
        raise Http404('Season does not exist')

    return render(request, 'plantbase/season_detail.html', context={'season': season})

def district_list_view_api(request):
    print("##################################################################")
    print("ListView Called")
    print("##################################################################")
    try:
        district_list = District.objects.all()
        print(district_list)
    except District.DoesNotExist:
        raise Http404('District does not exist')

    return render(request, 'plantbase/district_list.html', context={'district_list': district_list})

def district_detail_view_api(request):
    print("##################################################################")
    print("DetailView Called")
    print("##################################################################")
    _name = request.GET.get('name')
    try:
        district = District.objects.get(name=_name)
        print(district)
    except District.DoesNotExist:
        raise Http404('District does not exist')

    return render(request, 'plantbase/district_detail.html', context={'district': district})
"""
