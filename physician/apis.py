from django.shortcuts import get_object_or_404
from django.shortcuts import render
from plantbase.models import *
from django.views import generic
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *
from rest_framework.parsers import FileUploadParser
# from rest_framework.generics import (CreateAPIView)
from rest_framework.views import APIView
import threading
from .helper import *

# class upload_photo_api(CreateAPIView):
# 	serializer_class = ReportSerializer
# 	queryset = Report.objects.all()

class upload_photo_api(APIView):
	parser_class = (FileUploadParser,)

	def post(self, request, *args, **kwargs):

		report_serializer = ReportSerializer(data=request.data)

		if report_serializer.is_valid():
			report_serializer.save()
			t = threading.Thread(target=load_model_and_predict(report_serializer.data,"api"), args=(), kwargs={})
			t.setDaemon(True)
			t.start()
			return Response(report_serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(report_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def report_count_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        reports = Report.objects.all().count()
        return Response(reports)
    return Response("serializer.errors", status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def report_list_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        reports = Report.objects.all()
        serializer = ReportSerializer(reports, many=True)
        print("###########################################")
        print(serializer)
        print("###########################################")
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def report_detail_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        _id = request.GET.get('id')
        reports = Report.objects.get(id=_id)
        serializer = ReportSerializer(reports)
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def report_user_list_view_api(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        _uid = request.GET.get('uid')
        reports = Report.objects.filter(uid=_uid)
        serializer = ReportSerializer(reports, many=True)
        return Response(serializer.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
