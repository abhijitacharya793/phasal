# For models.
import torch
from torch import nn
from torchvision import datasets, transforms
from torchvision import models as torchmodels
import os,time
from .models import *
from torch import optim
import numpy as np
import matplotlib.pyplot as plt

from collections import OrderedDict
from PIL import Image
from pathlib import Path


#############################################
# Models.
# PATH = "/home/ubuntu/phasal/media/models/googlenet.pt"
PATH = "/home/katsuro/Documents/repo/fyp/phasal-aws/media/models/googlenet.pt"
layer_op = 1024

trans = transforms.Compose([
    transforms.RandomHorizontalFlip(),
    transforms.Resize(32),
    transforms.CenterCrop(32),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5),(0.5, 0.5, 0.5))
    ])

# image = Image.open(Path('media/diseasedpics/blast.jpg'))
# image = Image.open(Path('media/diseasedpics/blight.jpg'))
# image = Image.open(Path('media/diseasedpics/brownspot.jpg'))

#############################################


def load_model_and_predict(form,type):
    # print("In load model and predict")
    # print(form.cleaned_data['id'])
    # print(form.cleaned_data['uid'])
    # print(form.cleaned_data['title'])
    # print(form.cleaned_data['summary'])
    # print(form.cleaned_data['photo'])

    if type=="form":
        report = Report.objects.get(id=form.cleaned_data['id'])
        # image = Image.open(Path('/home/ubuntu/phasal/media/diseasedpics/'+str(form.cleaned_data['photo'])))
        image = Image.open(Path('/home/katsuro/Documents/repo/fyp/phasal-aws/media/diseasedpics/'+str(form.cleaned_data['photo'])))
    else:
        report = Report.objects.get(id=form['id'])
        # image = Image.open(Path('/home/ubuntu/phasal/'+str(form['photo'])))
        image = Image.open(Path('/home/katsuro/Documents/repo/fyp/phasal-aws/'+str(form['photo'])))

    model = torchmodels.googlenet(pretrained=True)
    for param in model.parameters():
        param.requires_grad = False

    classifier = nn.Sequential(OrderedDict([
                              ('fc1', nn.Linear(layer_op, 128)),
                              ('relu', nn.ReLU()),
                              ('dropout', nn.Dropout(p=0.2)),
                              ('fc2', nn.Linear(128, 3)),
                              ('output', nn.LogSoftmax(dim=1))
                              ]))
    model.fc = classifier
    model.load_state_dict(torch.load(PATH,map_location=torch.device('cpu')))
    model.eval()

    input = trans(image)

    input = input.view(1, 3, 32,32)

    output = model(input)

    prediction = int(torch.max(output.data, 1)[1].numpy())
    print('output : {} \nprediction : {}'.format(output.data,prediction))

    report.prediction = prediction
    report.save()
