from django.db import models
from django.urls import reverse # Used to generate URLs by reversing the URL patterns
import uuid # Required for unique instances

# Create your models here.
"""<<--__ NEWS __-->>"""
class News(models.Model):
    """Model representing a News Article."""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID for this particular article')
    meta_keyword = models.CharField(max_length=500, help_text='Enter Keywords')
    meta_last_modified = models.CharField(max_length=200, help_text='Enter Last Modified')
    meta_description = models.TextField(max_length=10000, help_text='Enter a brief description')
    title = models.CharField(max_length=200, help_text='Enter title')
    content = models.TextField(max_length=10000,default=None,help_text='Enter article content')
    date = models.CharField(max_length=50, help_text='Enter Date')
    weekday = models.CharField(max_length=50, help_text='Enter Weekday')
    month = models.CharField(max_length=50, help_text='Enter Month')
    SOURCE_NAME = (
        ('eco', 'Economic Times'),
        ('lm', 'LiveMint'),
        ('ndtv', 'NDTV'),
        ('toi', 'Times of India'),
    )
    source = models.CharField(
        max_length=4,
        choices=SOURCE_NAME,
        blank=True,
        null=True,
        default=None,
        help_text='Source of news',
    )

    def __str__(self):
        """String for representing the Model object."""
        return self.title

    def get_absolute_url(self):
        """Returns the url to access a detail record for this crop."""
        return reverse('news-detail')

    class Meta:
        ordering = ['title']
