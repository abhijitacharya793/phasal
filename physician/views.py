from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from .forms import *
import threading
from .helper import *

# Create your views here.
# Index
def index(request):
    return render(request, 'physician/index.html')

# Upload Photo
def upload_photo(request):

    if request.method == 'POST':
        form = ReportForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            t = threading.Thread(target=load_model_and_predict(form,"form"), args=(), kwargs={})
            t.setDaemon(True)
            t.start()
            return redirect('success')
    else:
        form = ReportForm()
    return render(request, 'physician/upload_photo.html', {'form' : form})

# Success page
def success(request):
    return render(request, 'physician/success.html')

# Report List View
def report_list_view(request):
    try:
        report_list = Report.objects.all()
    except Report.DoesNotExist:
        raise Http404('Report does not exist')

    return render(request, 'physician/reports.html', context={'report_list': report_list})

# Report Detail View
def report_detail_view(request):
    _id = request.GET.get('id')
    try:
        report = Report.objects.get(id=_id)
    except Report.DoesNotExist:
        raise Http404('Report does not exist')

    return render(request, 'physician/report_detail.html', context={'report': report})
